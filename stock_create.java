package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class stock_create {

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		// TODO Auto-generated method stub
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://192.168.23.16/seradatabase","root", "aeae");
		// DB connection
		Statement stmt = conn.createStatement();
		
		stmt.execute("create table Stock("
				
				+ "stock_stnd_iscd varchar(50) not null," //0 NOT_NULL 표준 종목코드
				+ "stock_date int not null," // 1 NOT_NULL 주식 영업 일자
				+ "stock_shrn_iscd varchar(50) not null," //2 NOT_NULL 유가증권 단축 종목코드 
				+ "stock_prpr int," // 3 주식 종가
				+ "stock_oprc int," // 4 주식 시가
				
				+ "stock_hgpr int," // 5 주식 최고가
				+ "stock_lwpr int," // 6 주식 최저가
				+ "stock_prdy_vrss_sign varchar(50)," // 7 전일 대비 부호 
				+ "stock_lprdy_vrss int," // 8 전일 대비
				+ "stock_prdy_ctrt float," // 9 전일 대비율
				
				+ "stock_prdy_vol double," // 10 전일 거래량
				+ "stock_acml_vol double," // 11 누적 거래량
				+ "stock_acml_tr_pbmn double," // 12 누적 거래 대금
				+ "stock_askp1 int," // 13 매도호가1 
				+ "stock_bidp1 int," // 14 매수호가1
				//
				+ "stock_total_askp_rsqn double," // 15 총 매도호가 잔량
				+ "stock_total_bidp_rsqn double," // 16 총 매수호가 잔량
				+ "stock_seln_cntg_smtn double," // 17 매도 체결 합계
				+ "stock_shnu_cntg_smtn double," // 18 매수 체결 합계
				+ "stock_seln_tr_pbmn double," // 19 매도 거래 대금(누적)
				
				+ "stock_shnu_tr_pbmn double," // 20 매수 거래 대금(누적)
				+ "stock_seln_cntg_csnu int," // 21 매도 체결 건수
				+ "stock_shnu_cntg_csnu int," // 22 매수 체결 건수
				+ "stock_w52_hgpr int," // 23 52주일 최고가
				+ "stock_w52_lwpr int," // 24 52주일 최저가
				
				+ "stock_w52_hgpr_date int," // 25 52주일 최고가 일자
				+ "stock_w52_lwpr_date int," // 26 52주일 최저가 일자
				+ "stock_ovtm_untp_bsop_hour int," // 27 시간외 단일가 최종 시간
				+ "stock_ovtm_untp_prpr int," // 28 시간외 단일가 현재가
				+ "stock_ovtm_untp_prdy_vrss int," // 29 시간외 단일가 전일 대비
				
				+ "stock_ovtm_untp_prdy_vrss_sign varchar(50)," // 시간외 단일가 전일 대비 부호 
				+ "stock_ovtm_untp_askp1 int," // 시간외 단일가 매도호가1
				+ "stock_ovtm_untp_bidp1 int," // 시간외 단일가 매수호가1
				+ "stock_ovtm_untp_vol double," // 시간외 단일가 거래량
				+ "stock_ovtm_untp_tr_pbmn double," // 시간외 단일가 거래 대금
				
				+ "stock_ovtm_untp_oprc int," // 시간외 단일가 시가
				+ "stock_ovtm_untp_hgpr int," // 시간외 단일가 최고가
				+ "stock_ovtm_untp_lwpr int," // 시간외 단일가 최저가
				+ "stock_mkob_otcp_vol double," // 장개시전 시간외종가 거래량
				+ "stock_mkob_otcp_tr_pbmn double," // 장개시전 시간외종가 거래 대금
				
				+ "stock_mkfa_otcp_vol double," // 장종료후 시간외종가 거래량
				+ "stock_mkfa_otcp_tr_pbmn double," // 장종료후 시간외종가 거래 대금
				+ "stock_mrkt_div_cls_code varchar(50)," // 시장 분류 구분 코드
				+ "stock_pstc_dvdn_amt double," // 주당 배당 금액
				+ "stock_lstn_stcn double," // 상장 주수
				
				+ "stock_stck_sdpr int," // 주식 기준가
				+ "stock_stck_fcam float," // 주식 액면가
				+ "stock_wghn_avrg_stck_prc double," // 가중 평균 주식 가격 
				+ "stock_issu_limt_rate float," // 종목 한도 비율 
				+ "stock_frgn_limt_qty double," // 외국인 한도 수량
				
				+ "stock_oder_able_qty double," // 주문 가능 수량 
				+ "stock_frgn_limt_exhs_cls_code varchar(50)," // 외국인 한도 소진 구분 코드
				+ "stock_frgn_hldn_qty double," // 외국인 보유 수량
				+ "stock_frgn_hldn_rate float," // 외국인 보유 비율
				+ "stock_hts_frgn_ehrt float," // HTS 외국인 소진율
				
				+ "stock_itmt_last_nav float," // 장중 최종 NAV 
				+ "stock_prdy_last_nav float," // 전일 최종 NAV
				+ "stock_trc_errt float," // 추적 오차율
				+ "stock_dprt float," //  괴리율
				+ "stock_ssts_cntg_qty double," //  공매도차입증권매도체결수량  
				
				+ "stock_ssts_tr_pbmn double," // 공매도차입증권매도거래대금
				+ "stock_stck_frgn_ntby_qty double," // 외국인 순매수
				+ "stock_flng_cls_code varchar(50)," //  락구분 코드 
				+ "stock_prtt_rate float," // 분할 비율  
				+ "stock_acml_prtt_rate float," // 누적 분할 비율
				
				+ "stock_stdv float," // 전체융자잔고비율
				+ "stock_beta_cfcn float," // 베타 계수(90일)  
				+ "stock_crlt_cfcn float," // DEL 상관 계수
				+ "stock_bull_beta float," // DEL 강세 계수 
				+ "stock_bear_beta float," // DEL 약세 계수
				
				+ "stock_bull_dvtn float,"                          //  DEL 강세 편차                               
		        + "stock_bear_dvtn float,"                          //  DEL 약세 편차                               
		        + "stock_bull_crlt float,"                          //  DEL 강세 상관계수                           
		        + "stock_bear_crlt float,"                          //  DEL 약세 상관계수                          
		        + "stock_stck_mxpr int,"                          //  주식 상한가                              
		        
		        + "stock_stck_llam int,"                          //  주식 하한가                                
		        + "stock_icic_cls_code varchar(50),"                 //  증자 구분 코드                           
		        + "stock_itmt_vol double,"                           //  장중 거래량                               
		        + "stock_itmt_tr_pbmn double,"                       //  장중 거래대금                              
		        + "stock_fcam_mod_cls_code varchar(50),"             //  액면가 변경 구분 코드                       
			   	
		        + "stock_revl_issu_reas_code varchar(50)," // 재평가 종목 사유 코드  
				+ "stock_orgn_ntby_qty double,"                           //  기관계 순매수   
			   	+ "stock_adj_prpr int,"                           //  수정주가   
			   	+ "stock_fn_oprc int,"                           //  주식 시가    
			   	+ "stock_fn_hgpr int,"                           //  주식 최고가   
			   	
			   	+ "stock_fn_lwpr int,"                           //  주식 최저가   
			   	+ "stock_fn_prpr int,"                           //  주식 종가   
			   	+ "stock_fn_acml_vol double,"                           //  누적 거래량   
			   	+ "stock_fn_acml_tr_pbmn double,"                           //  분할 비율    
			   	+ "stock_fn_prtt_rate float,"                           //  누적 거래 대금   
			   	
			   	+ "stock_fn_flng_cls_code varchar(50),"                           //  락구분 코드   
			   	+ "stock_buyin_nor_prpr int,"                           //  Buy-in 일반 체결가
			   	+ "stock_buyin_nor_prdy_vrss int,"                           //  Buy-in 일반 종가 대비
			   	+ "stock_buyin_nor_vol double,"                           //  Buy-in 일반 체결량
			   	+ "stock_buyin_nor_tr_pbmn double,"                           //  Buy-in 일반 체결 대금
//			   	
			   	+ "stock_buyin_tod_prpr int,"                           //  Buy-in 당일 체결가
			   	+ "stock_buyin_tod_prdy_vrss int,"                           //  Buy-in 당일 종가 대비
			   	+ "stock_buyin_tod_vol double,"                           //  Buy-in 당일 체결량
			   	+ "stock_buyin_tod_tr_pbmn double,"                           //  Buy-in 당일 체결 대금
			   	+ "primary key(stock_stnd_iscd,stock_shrn_iscd,stock_date));" // 코드+일자 복합기본키
				
				); 
		

		  		
		stmt.close();
		conn.close();
	}
}
