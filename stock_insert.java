package JDBC;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class stock_insert {
	public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException, Exception {
		//jdbc driver 클래스 로드
		Class.forName("com.mysql.jdbc.Driver");
		// DB connection
		Connection conn = DriverManager.getConnection("jdbc:mysql://192.168.23.16/seradatabase", "root", "aeae");
		// 자동 commit 끄기
		conn.setAutoCommit(false);
		// 쿼리생성
		Statement stmt = conn.createStatement();
		// 파일읽기
		File f = new File("C:\\Users\\남세라\\THTSKS010H00.dat");
		BufferedReader br = new BufferedReader(new FileReader(f));
		String readtxt;
		if ((readtxt = br.readLine()) == null) {
			System.out.printf("빈 파일입니다\n");
			return;
		}
		String[] field_name = readtxt.split(",");

		int LineCnt = 0;
		int Cnt = 1;
		while ((readtxt = br.readLine()) != null) {
			String[] field = readtxt.split("%_%");
			// 내용정제
			if (field.length > 2) {
				for (int i = 0; i < field.length; i++) {
					field[i] = field[i].replace("^", "").trim();
					if (field[i].length() == 0) {
						field[i] = null;
					}
				}
				String QueryTxt;

				QueryTxt = String.format("insert into Stock("
						+ "stock_stnd_iscd, stock_date, stock_shrn_iscd, stock_prpr, stock_oprc,"
						+ "stock_hgpr, stock_lwpr, stock_prdy_vrss_sign, stock_lprdy_vrss, stock_prdy_ctrt,"
						+ "stock_prdy_vol, stock_acml_vol, stock_acml_tr_pbmn, stock_askp1, stock_bidp1, "
						+ "stock_total_askp_rsqn, stock_total_bidp_rsqn, stock_seln_cntg_smtn, stock_shnu_cntg_smtn, stock_seln_tr_pbmn,"
						+ "stock_shnu_tr_pbmn, stock_seln_cntg_csnu, stock_shnu_cntg_csnu, stock_w52_hgpr, stock_w52_lwpr,"
						+ "stock_w52_hgpr_date, stock_w52_lwpr_date, stock_ovtm_untp_bsop_hour, stock_ovtm_untp_prpr, stock_ovtm_untp_prdy_vrss,"
						+ "stock_ovtm_untp_prdy_vrss_sign, stock_ovtm_untp_askp1, stock_ovtm_untp_bidp1, stock_ovtm_untp_vol, stock_ovtm_untp_tr_pbmn,"
						+ "stock_ovtm_untp_oprc, stock_ovtm_untp_hgpr, stock_ovtm_untp_lwpr, stock_mkob_otcp_vol, stock_mkob_otcp_tr_pbmn,"
						+ "stock_mkfa_otcp_vol, stock_mkfa_otcp_tr_pbmn, stock_mrkt_div_cls_code, stock_pstc_dvdn_amt, stock_lstn_stcn, "
						+ "stock_stck_sdpr, stock_stck_fcam, stock_wghn_avrg_stck_prc, stock_issu_limt_rate, stock_frgn_limt_qty,"
						+ "stock_oder_able_qty, stock_frgn_limt_exhs_cls_code, stock_frgn_hldn_qty, stock_frgn_hldn_rate, stock_hts_frgn_ehrt,"
						+ "stock_itmt_last_nav, stock_prdy_last_nav, stock_trc_errt, stock_dprt, stock_ssts_cntg_qty,"
						+ "stock_ssts_tr_pbmn, stock_stck_frgn_ntby_qty, stock_flng_cls_code, stock_prtt_rate, stock_acml_prtt_rate,"
						+ "stock_stdv, stock_beta_cfcn, stock_crlt_cfcn, stock_bull_beta, stock_bear_beta,"
						+ "stock_bull_dvtn, stock_bear_dvtn, stock_bull_crlt, stock_bear_crlt, stock_stck_mxpr,"
						+ "stock_stck_llam, stock_icic_cls_code, stock_itmt_vol, stock_itmt_tr_pbmn, stock_fcam_mod_cls_code,"
						+ "stock_revl_issu_reas_code,stock_orgn_ntby_qty, stock_adj_prpr, stock_fn_oprc, stock_fn_hgpr,"
						+ "stock_fn_lwpr, stock_fn_prpr, stock_fn_acml_vol, stock_fn_acml_tr_pbmn, stock_fn_prtt_rate,"
						+ "stock_fn_flng_cls_code, stock_buyin_nor_prpr, stock_buyin_nor_prdy_vrss, stock_buyin_nor_vol, stock_buyin_nor_tr_pbmn,   "
						+ "stock_buyin_tod_prpr, stock_buyin_tod_prdy_vrss, stock_buyin_tod_vol, stock_buyin_tod_tr_pbmn )"

						+ "values ("

						+ "'%s', %s, '%s', %s, %s," + " %s, %s, '%s', %s, %s," + "%s, %s, %s, %s, %s,"
						+ "%s, %s, %s, %s, %s," + "%s, %s, %s, %s, %s," + "%s, %s, %s, %s, %s,"
						+ "'%s', %s, %s, %s, %s,"

						+ "%s, %s, %s, %s, %s," + "%s, %s, '%s', %s, %s," + "%s, %s, %s, %s, %s,"
						+ "%s, '%s', %s, %s, %s," + "%s, %s, %s, %s, %s,"

						+ "%s, %s, '%s', %s, %s," + "%s, %s, %s, %s, %s," + "%s, %s, %s, %s, %s,"
						+ "%s, '%s', %s, %s, '%s'," + "'%s', %s, %s, %s, %s," + "%s, %s, %s, %s, %s,"
						+ "'%s', %s, %s, %s, %s," + "%s, %s, %s, %s" + ");", field[0], field[1], field[2], field[3],
						field[4], field[5], field[6], field[7], field[8], field[9], field[10], field[11], field[12],
						field[13], field[14], field[15], field[16], field[17], field[18], field[19], field[20],
						field[21], field[22], field[23], field[24], field[25], field[26], field[27], field[28],
						field[29], field[30], field[31], field[32], field[33], field[34], field[35], field[36],
						field[37], field[38], field[39], field[40], field[41], field[42], field[43], field[44],
						field[45], field[46], field[47], field[48], field[49], field[50], field[51], field[52],
						field[53], field[54], field[55], field[56], field[57], field[58], field[59], field[60],
						field[61], field[62], field[63], field[64], field[65], field[66], field[67], field[68],
						field[69], field[70], field[71], field[72], field[73], field[74], field[75], field[76],
						field[77], field[78], field[79], field[80], field[81], field[82], field[83], field[84],
						field[85], field[86], field[87], field[88], field[89], field[90], field[91], field[92],
						field[93], field[94], field[95], field[96], field[97], field[98]);

				// 쿼리 실행
				stmt.execute(QueryTxt);

				System.out.println(LineCnt++);
			}
		}
		conn.commit();
		conn.setAutoCommit(true);

		br.close();
		stmt.getClass();
		conn.close();
	}
}
