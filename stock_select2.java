package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class stock_select2 {

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://192.168.23.16:3306/seradatabase", "root", "aeae");
		Statement stmt = conn.createStatement();
		String Query = "select * from Stock where stock_date = 20130703;";
		ResultSet rset = stmt.executeQuery(Query);
		
		int LineCnt = 0;
		
		while(rset.next()) {
			
				System.out.println(rset.getString(2)+"  ");
				LineCnt++;
		}
		System.out.println(LineCnt);
		rset.close();
		stmt.close();
		conn.close();
	}
}
